FORMAT: 1A
HOST: http://localhost:3000

# Spark AS

Notes related resources of the **Plants API**

## Work Orders [/api/orders]
### Retrieve Orders [GET]
+ Parameters
    + id (optional,int) ... Name of the plant
   

+ Response 200 (application/json)

        [
            {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"",
                "cost" : 100
            
            }, {
                "id":3,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"",
                "cost" : 100
            
            }
        ]
        

### Create an Order [POST]
+ Request (application/json)

        {
           {
               
                "customer": {
                    "name":"john doe",
                    "email":"john@doe".com
                },
                "service":"service",
                "insurance": null
            
            }
        }

+ Response 201 (application/json)

    + Headers
    
            Location: http://192.168.99.100:3000/api/sales/orders/100

    + Body
    
           
            {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"",
                "cost" : 100
            
            }

+ Response 404 (application/json)

        {
            "message": "Order not found! (order id: 100)",
            "uri" : "http://192.168.99.100:3000/api/orders/order/100"
        }


       
### Modify an Order [PUT]



+ Request (application/json)

            {
                
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"some insurance"
            
            }
            

+ Response 200 (application/json)

    + Body
    
          
            {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"some insurance",
                "cost" : 100
            
            }
            
+ Response 404 (application/json)

        {
            "message": "The requested plant is not longer available",
            "uri" : "http://192.168.99.100:3000/api/sales/orders/100"
        }
        
### Close  Order [DELETE]

+ Request (application/json)

        {
            "_links":{"self":{"href":"http://192.168.99.100:3000/api/orders/order/100"}},
        }

+ Response 200 (application/json)

    + Body
    
            {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe.com"
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "insurance":"some insurance",
                "cost" : 100
            
            }



## Invoice [/api/invoices]

### Get all Invoice [GET]

+ Response 200 (application/json)

        [
          {
             
                "customer": {
                    "name":"john doe",
                    "email":"john@doe".com
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service",
                "total":200
            
            }
        ]

## Create Invoice via order id [/api/invoices/{id}]


### Create Invoice [POST] 

+ Request (application/json)

        {
            "_links":{"self":{"href":"http://192.168.99.100:3000/api/orders/order/100"}}
        }

+ Response 200 (application/json)

    + Body
    
           {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe".com
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service"
            
            }
            
## Remind Unpaid Invoice [/api/sales/invoices/{id}/remind]

Sends an invoice reminder via e-mail to buildit

### Create Invoice [POST] 

+ Request (application/json)

        {
            "_links":{"self":{"href":"http://192.168.99.100:3000/api/sales/invoices/100"}}
        }

+ Response 200 (application/json)

    + Body
    
            {
                "id":1,
                "customer": {
                    "name":"john doe",
                    "email":"john@doe".com
                },
                "vehicle":"Nissan"
                "mechanic": {
                    "name": "xxx"
                },
                "service":"service"
            
            }
